package ru.roman.services.impl

import org.junit.Test
import ru.roman.model.Person
import ru.roman.services.OutputService
import spock.lang.Specification

class XmlOutputServiceSpec extends Specification {

    OutputService service = new XmlOutputService()

    @Test
    def "throws error for null output data for creation"() {
        when:
        service.createOutput(null)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "throws error for null output data for writing"() {
        when:
        service.writeOutput(_ as String, null)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "throws error for null file name for writing"() {
        when:
        service.writeOutput(null, _ as String)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "creates some xml data"() {
        when:
        def result = service.createOutput([new Person("name": "JamesBond")])

        then:
        result
        result.contains("JamesBond")
        //here should be a lot of checks for xml tags
    }


}
