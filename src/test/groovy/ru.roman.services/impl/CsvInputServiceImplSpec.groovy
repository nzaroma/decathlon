package ru.roman.services.impl

import org.junit.Test
import ru.roman.services.CsvReaderService
import ru.roman.services.InputService
import spock.lang.Specification

class CsvInputServiceImplSpec extends Specification {
    InputService service

    CsvReaderService csvReader = Mock()

    def setup() {
        service = new CsvInputServiceImpl(csvReader)
    }

    @Test
    def "throws exception when service does not have file name as input"() {
        when:
        service.processInputFile(null)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "returns data when parsed successfully"() {
        given:
        csvReader.readCsv(_) >> [["someData"].toArray()]

        when:
        def result = service.processInputFile(_ as String)

        then:
        result
        result.size() == 1
    }
}
