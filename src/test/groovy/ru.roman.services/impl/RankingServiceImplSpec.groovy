package ru.roman.services.impl

import org.junit.Test
import ru.roman.model.Person
import ru.roman.services.RankingService
import spock.lang.Specification

class RankingServiceImplSpec extends Specification {
    RankingService service = new RankingServiceImpl()

    @Test
    def "sorts in ascending order"() {
        given:
        def input = [new Person("name": "winner", "totalScore": 9999), new Person("name": "looser", "totalScore": 7)]

        when:
        def result = service.rankPeople(input)

        then:
        result
        result.size() == 2
        result[0].name == "winner"
        result[0].position == "1"

        result[1].name == "looser"
        result[1].position == "2"
    }

    @Test
    def "shares the same place when scores are the same"() {
        given:
        def input = [new Person("name": "firstButNotVery", "totalScore": 9999), new Person("name": "alsoFirstButSecond", "totalScore": 9999)]

        when:
        def result = service.rankPeople(input)

        then:
        result
        result.size() == 2
        result[0].position == "1-2"
        result[1].position == "1-2"
    }

    @Test
    def "no players no prizes"() {
        given:
        def input = []

        when:
        def result = service.rankPeople(input)

        then:
        !result
    }
}
