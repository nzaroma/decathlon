package ru.roman.services.impl

import org.junit.Test
import ru.roman.services.DataProcessorService
import spock.lang.Specification

class DefaultDataProcessorServiceImplSpec extends Specification {

    DataProcessorService service = new DefaultDataProcessorServiceImpl()

    @Test
    def "returns empty collection for empty input"() {
        when:
        def result = service.process(null)

        then:
        !result
    }

    @Test
    def "throws exception when data is null"() {
        given:
        def input = [null]

        when:
        service.process(input)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "thrown row away if input length is not expected"() {
        given:
        def input = [["badData", "veryBad"] as String[]]

        when:
        def result = service.process(input)

        then:
        !result
    }

    @Test
    def "good data is processed"() {
        given:
        def input = [["name", "10.395", "7.76","18.4","2.20","46.17","13.8","56.17","5.28","77.19","3.53.79"] as String[]]
        def expectedPoints = 9992

        when:
        def result = service.process(input)

        then:
        result
        result.size() == 1
        result[0].name == "name"
        result[0].totalScore == expectedPoints
        //here should be a lot of checks that particular result goes to the right field
    }

    @Test
    def "thrown row away if input data has bad format"() {
        given:
        def input = [["name", "LETTERS_INSTEAD_OF_NUMBERS", "7.76","18.4","2.20","46.17","13.8","56.17","5.28","77.19","3.53.79"] as String[]]

        when:
        def result = service.process(input)

        then:
        !result
    }
}
