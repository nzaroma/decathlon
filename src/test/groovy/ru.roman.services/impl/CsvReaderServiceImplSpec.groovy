package ru.roman.services.impl

import org.junit.Test
import ru.roman.services.CsvReaderService
import spock.lang.Specification

class CsvReaderServiceImplSpec extends Specification{

    CsvReaderService service = new CsvReaderServiceImpl()

    @Test
    def "throws exception when file name is null"() {
        when:
        service.readCsv(null)

        then:
        thrown(IllegalArgumentException)
    }

    @Test
    def "parses successfully"() {
        when:
        def result = service.readCsv("/results_test.csv")

        then:
        result
        result.size() == 7
    }
}
