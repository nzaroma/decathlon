package ru.roman.services.impl;

import ru.roman.services.CsvReaderService;
import ru.roman.services.InputService;

import java.util.List;

import static ru.roman.util.ValidationUtil.validateParameterNotNullStandardMessage;

/**
 * Converts CSV file
 */
public class CsvInputServiceImpl implements InputService {

    private CsvReaderService csvReaderService;

    public CsvInputServiceImpl(CsvReaderService csvReaderService) {
        this.csvReaderService = csvReaderService;
    }

    @Override
    public List<String[]> processInputFile(String fileName) {
        validateParameterNotNullStandardMessage("fileName", fileName);
        return csvReaderService.readCsv(fileName);
    }
}
