package ru.roman.services.impl;

import ru.roman.services.CsvReaderService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ru.roman.util.ValidationUtil.validateParameterNotNullStandardMessage;

public class CsvReaderServiceImpl implements CsvReaderService {

    private static final String DELIMITER = ";";

    @Override
    public List<String[]> readCsv(String fileName) {
        validateParameterNotNullStandardMessage("fileName", fileName);
        final List<String[]> lines = new ArrayList<>();
        final String file = Optional.ofNullable(getClass().getResource(fileName))
                .map(URL::getFile)
                .orElseThrow(() -> new IllegalArgumentException("File " + fileName + " not found!"));
        try (final BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                final String[] lineValues = line.split(DELIMITER);
                lines.add(lineValues);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + fileName + " not found");
        } catch (IOException e) {
            System.out.println("Error while reading a file: ");
            e.printStackTrace();
        }
        return lines;
    }
}
