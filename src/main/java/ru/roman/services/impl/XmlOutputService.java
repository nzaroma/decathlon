package ru.roman.services.impl;

import ru.roman.model.Person;
import ru.roman.services.OutputService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static ru.roman.util.ValidationUtil.validateParameterNotNullStandardMessage;

/**
 * provides implementation for XML output service and writing to file
 */
public class XmlOutputService implements OutputService {

    private static final String XML_DEFINITION = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    private static final String OPEN_ANGLE_BRACKET = "<";
    private static final String OPEN_SLASH = "/";
    private static final String CLOSE_ANGLE_BRACKET = ">";
    private static final String NEW_LINE = System.lineSeparator();
    private static final String PERSON = "person";
    private static final String RESULTS = "results";
    private static final String NAME = "name";
    private static final String PLACE = "place";
    private static final String POINTS = "points";
    private static final String RUN_100_M = "run100m";
    private static final String LONG_JUMP = "longJump";
    private static final String SHOT_PUT = "shotPut";
    private static final String HIGH_JUMP = "highJump";
    private static final String RUN_400_M = "run400m";
    private static final String RUN_110_M_HURDLES = "run110mHurdles";
    private static final String DISCUS_THROW = "discusThrow";
    private static final String POLE_VAULT = "poleVault";
    private static final String JAVELIN_THROW = "javelinThrow";
    private static final String RUN_1500_M = "run1500m";

    @Override
    public String createOutput(List<Person> people) {
        validateParameterNotNullStandardMessage("people", people);
        final StringBuilder sb = new StringBuilder();
        sb.append(XML_DEFINITION).append(NEW_LINE);
        sb.append(createOpenTag(RESULTS)).append(NEW_LINE);
        sb.append(people.stream()
                .filter(Objects::nonNull)
                .map(this::wrap)
                .collect(Collectors.joining())
        );
        sb.append(createClosingTag(RESULTS)).append(NEW_LINE);

        return sb.toString();
    }

    private String wrap(Person person) {
        return createOpenTag(PERSON) + NEW_LINE + createPersonValue(person) + createClosingTag(PERSON) + NEW_LINE;
    }

    private String createPersonValue(Person person) {
        final StringBuilder sb = new StringBuilder();
        sb.append(createOpenTag(NAME)).append(person.getName()).append(createClosingTag(NAME)).append(NEW_LINE);
        sb.append(createOpenTag(PLACE)).append(person.getPosition()).append(createClosingTag(PLACE)).append(NEW_LINE);
        sb.append(createOpenTag(POINTS)).append(person.getTotalScore()).append(createClosingTag(POINTS)).append(NEW_LINE);

        sb.append(createOpenTag(RUN_100_M)).append(person.getResult100metres()).append(createClosingTag(RUN_100_M)).append(NEW_LINE);
        sb.append(createOpenTag(LONG_JUMP)).append(person.getResultLongJump()).append(createClosingTag(LONG_JUMP)).append(NEW_LINE);
        sb.append(createOpenTag(SHOT_PUT)).append(person.getResultShotPut()).append(createClosingTag(SHOT_PUT)).append(NEW_LINE);
        sb.append(createOpenTag(HIGH_JUMP)).append(person.getResultHighJump()).append(createClosingTag(HIGH_JUMP)).append(NEW_LINE);
        sb.append(createOpenTag(RUN_400_M)).append(person.getResult400metres()).append(createClosingTag(RUN_400_M)).append(NEW_LINE);
        sb.append(createOpenTag(RUN_110_M_HURDLES)).append(person.getResult110mHurdles()).append(createClosingTag(RUN_110_M_HURDLES)).append(NEW_LINE);
        sb.append(createOpenTag(DISCUS_THROW)).append(person.getResultDiscusThrow()).append(createClosingTag(DISCUS_THROW)).append(NEW_LINE);
        sb.append(createOpenTag(POLE_VAULT)).append(person.getResultPoleVault()).append(createClosingTag(POLE_VAULT)).append(NEW_LINE);
        sb.append(createOpenTag(JAVELIN_THROW)).append(person.getResultJavelinThrow()).append(createClosingTag(JAVELIN_THROW)).append(NEW_LINE);
        sb.append(createOpenTag(RUN_1500_M)).append(person.getResult1500metres()).append(createClosingTag(RUN_1500_M)).append(NEW_LINE);

        return sb.toString();
    }

    private String createClosingTag(String name) {
        return OPEN_ANGLE_BRACKET + OPEN_SLASH + name + CLOSE_ANGLE_BRACKET;
    }

    private String createOpenTag(String name) {
        return OPEN_ANGLE_BRACKET + name + CLOSE_ANGLE_BRACKET;
    }

    @Override
    public void writeOutput(String fileName, String data) {
        validateParameterNotNullStandardMessage("data", data);
        validateParameterNotNullStandardMessage("fileName", fileName);
        try {
            Files.write(Paths.get(fileName), data.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            System.out.println("Error while writing file " + e);
        }
    }
}
