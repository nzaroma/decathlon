package ru.roman.services.impl;

import ru.roman.model.Person;
import ru.roman.services.RankingService;

import java.util.*;
import java.util.stream.Collectors;

import static ru.roman.util.ValidationUtil.validateParameterNotNullStandardMessage;

public class RankingServiceImpl implements RankingService {


    @Override
    public List<Person> rankPeople(List<Person> people) { //TODO it should be prettier
        validateParameterNotNullStandardMessage("people", people);
        final Map<Integer, List<Person>> groupedByScore = groupByScore(people);
//        System.out.println(groupedByScore);

        final List<Map.Entry<Integer, List<Person>>> groupedByScoreSorted = sortGroupedByScorePeople(groupedByScore);

        int place = 0;
        List<Person> result = new ArrayList<>();
        for (Map.Entry<Integer, List<Person>> integerListEntry : groupedByScoreSorted) {
            String placeString;
            if (integerListEntry.getValue().size() == 1) {
                placeString = place + 1 + "";
                place++;
            } else {
                StringJoiner stringJoiner = new StringJoiner("-");

                for (int i = place; i < place + integerListEntry.getValue().size(); i++) {
                    stringJoiner.add(i + 1 + "");
                }
                place += integerListEntry.getValue().size();
                placeString = stringJoiner.toString();
            }
            integerListEntry.getValue().stream()
                    .peek(p -> p.setPosition(placeString))
                    .forEach(result::add);


        }
        return result;
    }

    private List<Map.Entry<Integer, List<Person>>> sortGroupedByScorePeople(Map<Integer, List<Person>> groupedByScore) {
        return groupedByScore.entrySet().stream()
                .sorted((o1, o2) -> o2.getKey().compareTo(o1.getKey()))
                .collect(Collectors.toList());
    }

    private Map<Integer, List<Person>> groupByScore(List<Person> people) {
        return people.stream().collect(Collectors.groupingBy(Person::getTotalScore));
    }
}
