package ru.roman.services.impl;

import ru.roman.model.CompetitionMapping;
import ru.roman.model.CompetitionType;
import ru.roman.model.Person;
import ru.roman.services.DataProcessorService;
import ru.roman.util.CollectionUtils;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ru.roman.util.ValidationUtil.validateParameterNotNullStandardMessage;


public class DefaultDataProcessorServiceImpl implements DataProcessorService {

    public static final int EXPECTED_LENGTH = 11;
    public static final int NAME_POSITION = 0;
    public static final int RESULT_100_M_POSITION = 1;
    private static final int RESULT_LONG_JUMP = 2;
    private static final int RESULT_SHOT_PUT = 3;
    public static final int RESULT_HIGH_JUMP = 4;
    public static final int RESULT_400_METRES = 5;
    public static final int RESULT_110_M_HURDLES = 6;
    public static final int RESULT_DISCUS_THROW = 7;
    public static final int RESULT_POLE_VAULT = 8;
    public static final int RESULT_JAVELIN_THROW = 9;
    public static final int RESULT_1500_METRES = 10;
    public static final int START_INDEX = 0;
    public static final int FINISH_INDEX = 10;


    @Override
    public List<Person> process(List<String[]> rows) {
        return CollectionUtils.emptyIfNull(rows).stream()
                .map(this::createAndCalculate)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Person createAndCalculate(String[] rowData) {
        validateParameterNotNullStandardMessage("rowData", rowData);
        if (rowData.length != EXPECTED_LENGTH) {
            System.out.println(MessageFormat.format("Throwing away row, because length {0} is not equal to expected {1}", rowData.length, EXPECTED_LENGTH));
            return null;
        }
        final Person person = new Person();
        person.setName(rowData[NAME_POSITION].trim());
        try {
            final int totalPoints = IntStream.range(START_INDEX, FINISH_INDEX)
                    .peek(index -> populatePersonResult(index, rowData[index + 1].trim(), person))
                    .map(index -> calculatePoints(index, rowData[index + 1].trim()))
                    .sum();
            person.setTotalScore(totalPoints);
//            System.out.println(totalPoints);
        } catch (RuntimeException e) {
            System.out.println(MessageFormat.format("Error during parsing/calculation results of person {0} " +
                    "\nThrowing it away, reason: {1}", person.getName(), e));
            return null;
        }

        return person;
    }

    private void populatePersonResult(int index, String value, Person person) {
        CompetitionMapping.values()[index].getPersonConsumer().accept(person, value);
    }

    private int calculatePoints(int index, String value) {
        final int result = calculate(value, CompetitionType.values()[index]);
        //System.out.println("index="+index+" value=" + value + "  result=" + result);
        return result;
    }

    private int calculate(String value, CompetitionType type) {
        return (int) Math.round(type.getA() * Math.pow(Math.abs(type.getTranslationFunction().getFunction().apply(value) - type.getB()), type.getC()));
    }
}

