package ru.roman.services;

import ru.roman.model.Person;

import java.util.List;

public interface RankingService {
    List<Person> rankPeople(List<Person> people);
}
