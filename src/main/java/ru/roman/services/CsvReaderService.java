package ru.roman.services;

import java.util.List;

public interface CsvReaderService {

    List<String[]> readCsv(String fileName);
}
