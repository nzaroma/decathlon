package ru.roman.services;

import ru.roman.model.Person;

import java.util.List;

public interface DataProcessorService {

    /**
     * Converts raw data from file to PersonResult model
     * Empty rows, rows with invalid/missing data would be thrown away
     *
     * @param rows
     * @return
     */
    List<Person> process(List<String[]> rows);
}
