package ru.roman.services;

import java.util.List;

public interface InputService {

    /**
     * Convert file content to the expected format
     * @param fileName
     * @return
     */
    List<String[]> processInputFile(String fileName);
}
