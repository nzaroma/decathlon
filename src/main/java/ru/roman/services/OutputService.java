package ru.roman.services;

import ru.roman.model.Person;

import java.util.List;

/**
 * service to write/save result
 */
public interface OutputService {

    String createOutput(List<Person> people);

    void writeOutput(String fileName, String data);
}
