package ru.roman.util;

import java.text.MessageFormat;

public class ValidationUtil {

    public static void validateParameterNotNullStandardMessage(String name, Object parameter) {
        if (parameter == null) {
            throw new IllegalArgumentException(MessageFormat.format("{0} should not be null!", name));
        }
    }
}
