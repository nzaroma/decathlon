package ru.roman.util;

import java.util.Collections;
import java.util.List;

public class CollectionUtils {

    public static <T> List<T> emptyIfNull(List<T> list) {
        return list == null
                ? Collections.emptyList()
                : list;

    }
}
