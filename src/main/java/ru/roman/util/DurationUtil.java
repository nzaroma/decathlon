package ru.roman.util;

import java.time.Duration;

public class DurationUtil {

    public static final String DURATION_REGEX = "\\.";
    public static final int MILLIS_FACTOR = 10;

    public static Duration createDuration(String input) {
        ValidationUtil.validateParameterNotNullStandardMessage("input", input);
        final String[] splittedData = input.split(DURATION_REGEX);
        final int length = splittedData.length;
        if (length == 0) {
            throw new IllegalArgumentException("Can not parse input duration " + input);
        }
        String ms = splittedData[length - 1];
        Duration duration;
        if (ms.length() == 1) {
            duration = Duration.ofMillis(100 * Long.valueOf(ms));
        } else if (ms.length() == 2) {
            duration = Duration.ofMillis(MILLIS_FACTOR * Long.valueOf(ms));
        } else if (ms.length() == 3) {
            duration = Duration.ofMillis(Long.valueOf(ms));
        } else {
            duration =  Duration.ofMillis(MILLIS_FACTOR * Long.valueOf(ms.substring(0, 3)));
        }

        if (length > 1) {
            duration = duration.plus(Duration.ofSeconds(Long.valueOf(splittedData[length - 2])));
        }
        if (length > 2) {
            duration = duration.plus(Duration.ofMinutes(Long.valueOf(splittedData[length - 3])));
        }
        if (length > 3) {
            duration = duration.plus(Duration.ofHours(Long.valueOf(splittedData[length - 4])));
        }
        return duration;
    }
}
