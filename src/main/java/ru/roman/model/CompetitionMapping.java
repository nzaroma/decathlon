package ru.roman.model;

import java.util.function.BiConsumer;

/**
 * relation between competition name and setter methods
 */
public enum CompetitionMapping {

    RUN_100M(Person::setResult100metres),
    LONG_JUMP(Person::setResultLongJump),
    SHOT_PUT(Person::setResultShotPut),
    HIGH_JUMP(Person::setResultHighJump),
    RUN_400M(Person::setResult400metres),
    RUN_110M_HURDLES(Person::setResult110mHurdles),
    DISCUS_THROW(Person::setResultDiscusThrow),
    POLE_VAULT(Person::setResultPoleVault),
    JAVELIN_THROW(Person::setResultJavelinThrow),
    RUN_1500M(Person::setResult1500metres);

    BiConsumer<Person, String> consumer;

    CompetitionMapping(BiConsumer<Person, String> consumer) {
        this.consumer = consumer;
    }

    public BiConsumer<Person, String> getPersonConsumer() {
        return consumer;
    }
}
