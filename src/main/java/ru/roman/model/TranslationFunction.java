package ru.roman.model;

import ru.roman.util.DurationUtil;

import java.util.function.Function;

/**
 * Determines how String should be mapped to Double
 */
public enum TranslationFunction {
    DURATION {
        @Override
        public Function<String, Double> getFunction() {
            return text -> (double) DurationUtil.createDuration(text).toMillis() / 1000;
        }
    },
    DISTANCE {
        @Override
        public Function<String, Double> getFunction() {
            return Double::valueOf;
        }
    };

    public abstract Function<String, Double> getFunction();
}
