package ru.roman.model;

/**
 * enum that helps us calculate points, consists of coefficients and translation rules (string -> double)
 * coefficients are taken from ru-wiki, because en-wiki coefficients are not normalized (in one situation they are for metres, in other for centimeters and so on).
 * coefficients are checked with en-wiki 1000 points referenced numbers
 */
public enum CompetitionType {

    RUN_100M(25.4347, 18, 1.81, TranslationFunction.DURATION),
    LONG_JUMP(90.5674, 2.2, 1.4, TranslationFunction.DISTANCE),
    SHOT_PUT(51.39, 1.5, 1.05, TranslationFunction.DISTANCE),
    HIGH_JUMP(585.64, 0.75, 1.42, TranslationFunction.DISTANCE),
    RUN_400M(1.53775, 82, 1.81, TranslationFunction.DURATION),
    RUN_110M_HURDLES(5.74354, 28.5, 1.92, TranslationFunction.DURATION),
    DISCUS_THROW(12.91, 4, 1.1, TranslationFunction.DISTANCE),
    POLE_VAULT(140.182, 1, 1.35, TranslationFunction.DISTANCE),
    JAVELIN_THROW(10.14, 7, 1.08, TranslationFunction.DISTANCE),
    RUN_1500M(0.03768, 480, 1.85, TranslationFunction.DURATION);

    private double a;
    private double b;
    private double c;
    private TranslationFunction translationFunction;

    CompetitionType(double a, double b, double c, TranslationFunction translationFunction) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.translationFunction = translationFunction;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public TranslationFunction getTranslationFunction() {
        return translationFunction;
    }
}

/*
from en wiki

    RUN_100M(25.4347, 18, 1.81, TranslationFunction.DURATION),
    LONG_JUMP(0.14354, 220, 1.4, TranslationFunction.DISTANCE),
    SHOT_PUT(51.39, 1.5, 1.05, TranslationFunction.DISTANCE),
    HIGH_JUMP(0.8465, 75, 1.42, TranslationFunction.DISTANCE),
    RUN_400M(1.53775, 82, 1.81, TranslationFunction.DURATION),
    RUN_110M_HURDLES(5.74352, 28.5, 1.92, TranslationFunction.DURATION),
    DISCUS_THROW(12.91, 4, 1.1, TranslationFunction.DISTANCE),
    POLE_VAULT(0.2797, 100, 1.35, TranslationFunction.DISTANCE),
    JAVELIN_THROW(10.14, 7, 1.08, TranslationFunction.DISTANCE),
    RUN_1500M(0.03768, 480, 1.85, TranslationFunction.DURATION);
 */