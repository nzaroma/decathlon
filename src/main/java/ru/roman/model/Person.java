package ru.roman.model;

public class Person {

    private String name;
    private String result100metres;
    private String resultLongJump;
    private String resultShotPut;
    private String resultHighJump;
    private String result400metres;
    private String result110mHurdles;
    private String resultDiscusThrow;
    private String resultPoleVault;
    private String resultJavelinThrow;
    private String result1500metres;
    private int totalScore;
    private String position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResult100metres() {
        return result100metres;
    }

    public void setResult100metres(String result100metres) {
        this.result100metres = result100metres;
    }

    public String getResultLongJump() {
        return resultLongJump;
    }

    public void setResultLongJump(String resultLongJump) {
        this.resultLongJump = resultLongJump;
    }

    public String getResultShotPut() {
        return resultShotPut;
    }

    public void setResultShotPut(String resultShotPut) {
        this.resultShotPut = resultShotPut;
    }

    public String getResultHighJump() {
        return resultHighJump;
    }

    public void setResultHighJump(String resultHighJump) {
        this.resultHighJump = resultHighJump;
    }

    public String getResult400metres() {
        return result400metres;
    }

    public void setResult400metres(String result400metres) {
        this.result400metres = result400metres;
    }

    public String getResult110mHurdles() {
        return result110mHurdles;
    }

    public void setResult110mHurdles(String result110mHurdles) {
        this.result110mHurdles = result110mHurdles;
    }

    public String getResultDiscusThrow() {
        return resultDiscusThrow;
    }

    public void setResultDiscusThrow(String resultDiscusThrow) {
        this.resultDiscusThrow = resultDiscusThrow;
    }

    public String getResultPoleVault() {
        return resultPoleVault;
    }

    public void setResultPoleVault(String resultPoleVault) {
        this.resultPoleVault = resultPoleVault;
    }

    public String getResultJavelinThrow() {
        return resultJavelinThrow;
    }

    public void setResultJavelinThrow(String resultJavelinThrow) {
        this.resultJavelinThrow = resultJavelinThrow;
    }

    public String getResult1500metres() {
        return result1500metres;
    }

    public void setResult1500metres(String result1500metres) {
        this.result1500metres = result1500metres;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", result100metres='" + result100metres + '\'' +
                ", resultLongJump='" + resultLongJump + '\'' +
                ", resultShotPut='" + resultShotPut + '\'' +
                ", resultHighJump='" + resultHighJump + '\'' +
                ", result400metres='" + result400metres + '\'' +
                ", result110mHurdles='" + result110mHurdles + '\'' +
                ", resultDiscusThrow='" + resultDiscusThrow + '\'' +
                ", resultPoleVault='" + resultPoleVault + '\'' +
                ", resultJavelinThrow='" + resultJavelinThrow + '\'' +
                ", result1500metres='" + result1500metres + '\'' +
                ", totalScore=" + totalScore +
                ", position='" + position + '\'' +
                '}';
    }
}
