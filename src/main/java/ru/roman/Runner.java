package ru.roman;

import ru.roman.model.Person;
import ru.roman.services.DataProcessorService;
import ru.roman.services.InputService;
import ru.roman.services.OutputService;
import ru.roman.services.RankingService;
import ru.roman.services.impl.*;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        System.out.println("Starting");
        final InputService inputService = new CsvInputServiceImpl(new CsvReaderServiceImpl());
        final List<String[]> rawData = inputService.processInputFile("/results.csv");
//        System.out.println(rawData);

        final DataProcessorService processorService = new DefaultDataProcessorServiceImpl();
        final List<Person> people = processorService.process(rawData);
//        System.out.println(people);

        final RankingService rankingService = new RankingServiceImpl();
        final List<Person> sortedPeople = rankingService.rankPeople(people);
//        System.out.println(sortedPeople);

        final OutputService outputService = new XmlOutputService();
        final String output = outputService.createOutput(sortedPeople);
//        System.out.println(output);
        outputService.writeOutput("output.xml", output);

        System.out.println("Finishing");
    }
}
